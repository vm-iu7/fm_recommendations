USE mr_recommendations;

# --- !Ups

CREATE TABLE RATINGS (
  user_id INT NOT NULL,
  movie_id INT NOT NULL,
  rating FLOAT NOT NULL,
  PRIMARY KEY (user_id, movie_id)
);

CREATE TABLE RECOMMENDATIONS (
  user_id INT,
  movie_id INT,
  weight REAL,
  PRIMARY KEY (user_id, movie_id)
);

# --- !Downs

DROP TABLE RATINGS;
DROP TABLE RECOMMENDATIONS;