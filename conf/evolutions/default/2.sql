USE mr_recommendations;

# --- !Ups

CREATE INDEX index_ratings_userid ON RATINGS (user_id);
CREATE INDEX index_ratings_movieid ON RATINGS (movie_id);

CREATE INDEX index_recommendations_user_id ON RECOMMENDATIONS (user_id);
CREATE INDEX index_recommendations_movie_id ON RECOMMENDATIONS (movie_id);
