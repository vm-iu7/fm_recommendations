name := """fm_recommendations"""

version := "0.1.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.6"

ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) }

libraryDependencies ++= Seq(
  cache,
  ws,
  evolutions,
  specs2 % Test,

  "org.scala-lang.modules" % "scala-async_2.11" % "0.9.5",

  "com.github.tototoshi" %% "scala-csv" % "1.2.2",

  "org.apache.spark" %% "spark-core" % "1.5.1",
  "org.apache.spark" %% "spark-mllib" % "1.5.1",
  "org.apache.spark" %% "spark-hive" % "1.5.1",

  "com.h2database" % "h2" % "1.4.189",

  "mysql" % "mysql-connector-java" % "5.1.36",

  "com.typesafe.play" %% "play-slick" % "1.1.1",
  "com.typesafe.play" %% "play-slick-evolutions" % "1.1.1",

  "com.github.tototoshi" %% "slick-joda-mapper" % "2.1.0"
)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

dependencyOverrides ++= Set(
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.4.4"
)

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator

javaOptions in Universal ++= Seq(
  // JVM memory tuningcha
  "-J-Xmx512m",
  "-J-Xms256m",

  // Since play uses separate pidfile we have to provide it with a proper path
  s"-Dpidfile.path=/var/run/${packageName.value}/play.pid",

  // Use separate configuration file for production environment
  s"-Dconfig.file=/usr/share/${packageName.value}/conf/production.conf",

  // Use separate logger configuration file for production environment
  s"-Dlogger.file=/usr/share/${packageName.value}/conf/production-logger.xml",

  // You may also want to include this setting if you use play evolutions
  "-DapplyEvolutions.default=true"
)

daemonUser in Linux := "findmovie"
maintainer in Linux := "First Lastname <first.last@example.com>"
packageSummary in Linux := "My custom package summary"
packageDescription := "My longer package description"
rpmRelease := "11"
rpmVendor := "example.com"
rpmUrl := Some("http://github.com/example/server")
rpmLicense := Some("Apache v2")
