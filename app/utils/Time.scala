package utils


object Time {
  def timeMeasured[A](block: => A): (A, Long) = {
    val start = System.currentTimeMillis()
    val res = block
    val stop = System.currentTimeMillis()
    val diff = stop - start
    (res, diff)
  }
}
