package models

import akka.actor.ActorSystem
import com.google.inject.Inject
import play.api.Logger
import play.api.inject.ApplicationLifecycle
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._

import utils.Time._

class RetrainJob @Inject() (
  recEngine: RecommendationEngine,
  system: ActorSystem,
  applicationLifecycle: ApplicationLifecycle
) (implicit ec: ExecutionContext) {

  val cancellable = system.scheduler.schedule(5.seconds, 3.minutes, new Runnable {
    override def run(): Unit = {
      val (_, time) = timeMeasured { recEngine.retrain() }
      Logger.info(s"Retrained in $time ms")
    }
  })

  applicationLifecycle.addStopHook(() => {
    Future(cancellable.cancel())
  })

}
