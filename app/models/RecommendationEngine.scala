package models

import com.google.inject.Inject
import org.apache.spark.mllib.recommendation.{ALS, MatrixFactorizationModel}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext, mllib}
import play.api.inject.ApplicationLifecycle
import play.api.{Configuration, Logger}
import utils.Time._

import scala.async.Async._
import scala.concurrent.{ExecutionContext, Future}


class RecommendationEngine @Inject() (
  ratingDAO: RatingDAO,
  recommendationsDAO: RecommendationsDAO,
  config: Configuration,
  applicationLifecycle: ApplicationLifecycle
) (implicit ec: ExecutionContext) {

  private case class DatabaseConfiguration(
    driver: String,
    url: String,
    dbtable: String,
    user: String,
    password: String
  )

  private val databaseConfig = (for {
    driver <- config.getString("slick.dbs.default.db.driver")
    url <- config.getString("slick.dbs.default.db.url")
    user <- config.getString("slick.dbs.default.db.user")
    password <- config.getString("slick.dbs.default.db.password")
  } yield {
    new DatabaseConfiguration(driver, url, "RATINGS", user, password)
  }).get

  val sparkConf = new SparkConf()
    .setAppName("fm_recommendations")
    .setMaster("local[*]")

  val sparkContext = new SparkContext(sparkConf)

  val sparkSqlContext = new SQLContext(sparkContext)

  applicationLifecycle.addStopHook { () =>
    Future(sparkContext.stop())
  }

  val df = loadDataframeFromRatings()

  val ratingsRDD: RDD[mllib.recommendation.Rating] = df.rdd.map { row =>
    val userId = row.getAs[Int]("user_id")
    val movieId = row.getAs[Int]("movie_id")
    val rating = row.getAs[Double]("rating")
    mllib.recommendation.Rating(userId, movieId, rating)
  }

  var alsModel = Option.empty[MatrixFactorizationModel]

  def mostPopularMovies(topCount: Int, minRatings: Int) (implicit ec: ExecutionContext)
  : Seq[Recommendation] = {

    val groupedByMovie = ratingsRDD.groupBy(_.product)

    val withEnoughRatings = groupedByMovie.filter {
      case (_, movieGroup) => movieGroup.size > minRatings
    }

    val movieAvgRatings = withEnoughRatings.map {
      case (movieId: Int, ratingsGroup: Iterable[mllib.recommendation.Rating]) =>
        val sum: Double = ratingsGroup.map(_.rating).sum
        val count: Int = ratingsGroup.count(r => true)

        movieId -> sum / count.asInstanceOf[Double]
    }

    val topMovies = movieAvgRatings
      .takeOrdered(topCount)(Ordering[Double].reverse.on {
        case (movieId, avgRating) => avgRating
      })

    topMovies
      .map { case (movieId, avgRating) => Recommendation(0, movieId, avgRating) }
  }

  def getUserRecommendations(userId: Int)(implicit ec: ExecutionContext)
  : Future[Iterable[Recommendation]] = {
    alsModel match {
      case Some(m) => Future {
        m.recommendProducts(userId, 25)
          .map(p => Recommendation(p.user, p.product, p.rating))
      }
      case None => Future {
        Array.empty[Recommendation]
      }
    }
  }

  def retrain(): Future[Unit] = async {
    val f = async {
      Logger.info("Training ALS...")
      val (model, time) = timeMeasured {
        ALS.train(ratingsRDD, 100, 10)
      }
      Logger.info(s"Trained ALS in $time ms")

      model
    }
    val newModel = await(f)
    alsModel = Some(newModel)
  }

  private def loadDataframeFromRatings() = {
    sparkSqlContext.read.format("jdbc").options(
      Map(
        "driver"    -> databaseConfig.driver,
        "url"       -> databaseConfig.url,
        "dbtable"   -> databaseConfig.dbtable,
        "user"      -> databaseConfig.user,
        "password"  -> databaseConfig.password
      )
    ).load()
  }
}
