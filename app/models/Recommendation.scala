package models

import javax.inject.{Inject, Singleton}

import com.typesafe.config.Config
import play.api.db.slick.{HasDatabaseConfig, DatabaseConfigProvider}
import play.api.libs.json.Json
import slick.driver.JdbcProfile

import scala.concurrent.{Future, ExecutionContext}


case class Recommendation(userId: Int, movieId: Int, weight: Double)
object Recommendation {
  implicit val jsonFormat = Json.format[Recommendation]
}

@Singleton
class RecommendationsDAO @Inject() (protected val config: Config,
                                    protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfig[JdbcProfile] {

  import driver.api._

  protected val dbConfig = dbConfigProvider.get[JdbcProfile]

  private val table = TableQuery[RecTable]

  def lookup(userId: Int, movieId: Int)
            (implicit ec: ExecutionContext): Future[Option[Recommendation]] = {
    dbConfig.db.run {
      table.filter(r => (r.userId === userId) && (r.movieId === movieId))
           .result.headOption
    }
  }

  def forUser(userId: Int) (implicit ec: ExecutionContext): Future[Seq[Recommendation]] = {
    dbConfig.db.run {
      table.filter(r => r.userId === userId).sortBy(r => r.weight.desc).result
    }
  }

  def create(userId: Int, movieId: Int, weight: Double)
            (implicit ec: ExecutionContext): Future[Recommendation] = {
    val rec = Recommendation(userId, movieId, weight)
    dbConfig.db.run(table += rec).map(_ => rec)
  }

  def createOrUpdate(userId: Int, movieId: Int, weight: Double)
                    (implicit ec: ExecutionContext): Future[Recommendation] = {
    val rec = Recommendation(userId, movieId, weight)
    dbConfig.db.run(table.insertOrUpdate(rec)).map(_ => rec)
  }

  def deleteAll()(implicit ec: ExecutionContext): Future[Unit] = {
    dbConfig.db.run(table.delete)
               .map(_ => Unit)
  }

  private class RecTable(tag: Tag) extends Table[Recommendation](tag, "RECOMMENDATIONS") {
    def userId = column[Int]("user_id")
    def movieId = column[Int]("movie_id")
    def weight = column[Double]("weight")

    def pk = primaryKey("pk_recommendations", (userId, movieId))

    def * = (userId, movieId, weight) <> ((Recommendation.apply _).tupled, Recommendation.unapply)
  }

}