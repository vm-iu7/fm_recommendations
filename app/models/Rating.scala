package models

import java.util.NoSuchElementException
import javax.inject.{Inject, Singleton}

import com.typesafe.config.Config
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfig}
import play.api.libs.json.Json
import slick.driver.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}


case class Rating(userId: Int, movieId: Int, rating: Double)

object Rating {
  implicit val jsonFormat = Json.format[Rating]
}

@Singleton
class RatingDAO @Inject() (protected val config: Config,
                           protected val dbConfigProvider: DatabaseConfigProvider)
extends HasDatabaseConfig[JdbcProfile] {

  import driver.api._

  protected val dbConfig = dbConfigProvider.get[JdbcProfile]

  private val ratings = TableQuery[RatingsTable]

  def lookup(userId: Int, movieId: Int)
            (implicit ec: ExecutionContext): Future[Option[Rating]] = {
    dbConfig.db.run(
      ratings.filter(r => (r.userId === userId) && (r.movieId === movieId))
             .result.headOption
    )
  }

  def ofUser(userId: Int)
            (implicit ec: ExecutionContext): Future[Seq[Rating]] = {
    dbConfig.db.run(ratings.filter(r => r.userId === userId).result)
  }

  def ofUser(userId: Int, movieList: Seq[Int])
            (implicit ec: ExecutionContext): Future[Seq[Rating]] = {
    dbConfig.db.run(ratings.filter(r => (r.userId === userId) && (r.movieId inSet movieList)).result)
  }

  def create(userId: Int, movieId: Int, rating: Double)
            (implicit ec: ExecutionContext): Future[Rating] = {
    val r = Rating(userId, movieId, rating)
    dbConfig.db.run(ratings.insertOrUpdate(r)).map(s => r)
  }

  def update(userId: Int, movieId: Int, rating: Double)
            (implicit ec: ExecutionContext): Future[Rating] = {
    val rowsAffected = dbConfig.db.run {
      ratings.filter(r => r.userId === userId && r.movieId === movieId)
        .map(_.rating)
        .update(rating)
    }

    rowsAffected.map {
      case 0 => throw new NoSuchElementException(s"No rating with key ($userId, $movieId)")
      case 1 => Rating(userId, movieId, rating)
      case n => throw new RuntimeException(s"Expected 0 or 1 change, but got $n")
    }
  }

  def delete(userId: Int, movieId: Int)
            (implicit ec: ExecutionContext): Future[Unit] = {
    val rowsAffected = dbConfig.db.run {
      ratings.filter(r => r.userId === userId && r.movieId === movieId)
        .delete
    }

    rowsAffected.map {
      case 0 => throw new NoSuchElementException(s"No rating with key ($userId, $movieId)")
      case 1 => Unit
      case n => throw new RuntimeException(s"Expected 0 or 1 change, but got $n")
    }
  }

  def insertAll(data: Iterable[Rating])
               (implicit ec: ExecutionContext): Future[Int] = {
    dbConfig.db
      .run(ratings ++= data)
      .map(s => data.size)
  }

  private class RatingsTable(tag: Tag) extends Table[Rating](tag, "RATINGS") {
    def userId = column[Int]("user_id")
    def movieId = column[Int]("movie_id")
    def rating = column[Double]("rating")

    def pk = primaryKey("pk_ratings", (userId, movieId))

    def * = (userId, movieId, rating) <> ((Rating.apply _).tupled, Rating.unapply)
  }

}
