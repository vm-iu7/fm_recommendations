package modules

import com.google.inject.AbstractModule
import play.api.{Environment, Configuration}

import com.typesafe.config.Config


class AppModule(environment: Environment,
                     configuration: Configuration) extends AbstractModule {
  override def configure(): Unit = {
    bind(classOf[Config]).toInstance(configuration.underlying)
    bind(classOf[models.RatingDAO]).asEagerSingleton()
    bind(classOf[models.RecommendationEngine]).asEagerSingleton()
    bind(classOf[models.RetrainJob]).asEagerSingleton()
  }
}
