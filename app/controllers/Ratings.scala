package controllers

import javax.inject.Inject

import com.typesafe.config.Config

import controllers.utils.{AccessLogging, Error}

import play.api.libs.json.{JsError, Json}
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits._

import com.github.tototoshi.csv.CSVReader

import models.{Rating, RatingDAO}

import scala.concurrent.Future
import scala.util.Try


class Ratings @Inject() (
  config: Config,
  ratings: RatingDAO
) extends Controller with AccessLogging {

  def get(userId: Int, movieId: Int) = Action.async {
    ratings.lookup(userId, movieId)
      .map(o => o.map(r => Ok(Json.toJson(r)))
                 .getOrElse(NotFound(Json.obj("error" -> s"Not found rating corresponding to pair $userId -> $movieId"))))
      .recover {
        case e => InternalServerError(Json.obj("error" -> e.getMessage))
      }
  }

  def forUser(userId: Int) = Action.async {
    ratings.ofUser(userId).map { ratings =>
      Ok(Json.toJson(ratings))
    }
  }


  def userRatings(userId: Int, movies: Option[Seq[Int]]) = movies match {
    case Some(x) => forUserMovies(userId, x)
    case None => forUser(userId)
  }


  def forUserMovies(userId: Int, movies: Seq[Int]) = Action.async {
    ratings.ofUser(userId, movies).map { rlist =>
      Ok(Json.toJson(rlist))
    }
  }


  case class CreateParams(userId: Int, movieId: Int, rating: Double)
  implicit val createParamsJsonFormat = Json.format[CreateParams]

  def create() = Action.async(parse.json) { req =>

    req.body.validate[CreateParams].map { params =>

      ratings.create(params.userId, params.movieId, params.rating).map { rating =>
        Ok(Json.toJson(rating))
      }.recover {
        case e => InternalServerError(Json.toJson(Error(e.getMessage)))
      }

    }.recoverTotal {
      case e: JsError => Future(
        BadRequest(Json.obj(
          "error" -> "Failed to parse body",
          "details" -> JsError.toJson(e)
        ))
      )
    }
  }

  def delete(userId: Int, movieId: Int) = Action.async {
    ratings.delete(userId, movieId).map { _ =>
      Ok(Json.obj("result" -> s"successfully deleted rating ($userId, $movieId)"))
    }.recover {
      case e: NoSuchElementException => NotFound(Json.obj("error" -> e.getMessage))
      case e => InternalServerError(Json.obj("error" -> e.getMessage))
    }
  }

  case class UpdateParams(rating: Double)
  implicit val updateParamsJsonFormat = Json.format[UpdateParams]

  def update(userId: Int, movieId: Int) = Action.async(parse.json) { req =>
    req.body.validate[UpdateParams].map { params =>
      ratings.create(userId, movieId, params.rating)
        .map { rating => Ok(Json.toJson(rating)) }
        .recover {
          case e: NoSuchElementException => NotFound(Json.obj("error" -> e.getMessage))
          case e => InternalServerError(Json.obj("error" -> e.getMessage))
        }
    }.recoverTotal {
      case e:JsError => Future {
        BadRequest(Json.obj(
          "error" -> "Failed to parse body",
          "details" -> JsError.toJson(e)
        ))
      }
    }
  }

  def upload() = Action.async(parse.multipartFormData) { implicit req =>
    def ratingFromFields(
      userIdField: String,
      movieIdField: String,
      ratingField: String
    ): Try[Rating] = {
      for {
        userId <- Try(userIdField.toInt)
        movieId <- Try(movieIdField.toInt)
        rating <- Try(ratingField.toDouble)
      } yield {
        new Rating(userId, movieId, rating)
      }
    }

    def processRecord(fields: Seq[String]): Option[Rating] = {
      val lift: Int => Option[String] = fields.lift
      val optTry = for {
        userIdField <- lift(0)
        movieIdField <- lift(1)
        ratingField <- lift(2)
      } yield {
        ratingFromFields(userIdField, movieIdField, ratingField).toOption
      }
      optTry.flatten
    }

    req.body.file("ratings")
      .map { ratingsFile =>
        val csvReader = CSVReader.open(ratingsFile.ref.file)
        val ratingsWithFailed = csvReader.iterator.map(processRecord)
        val ratingsParsed = ratingsWithFailed.filter(r => r.isDefined).map(r => r.get)
        ratings.insertAll(ratingsParsed.toList)
               .map(insertedCount => Ok(Json.obj("ok" -> insertedCount)))
      }
      .getOrElse(Future(BadRequest("No 'ratings' file")))
  }

}
