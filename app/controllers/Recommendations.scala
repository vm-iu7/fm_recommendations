package controllers

import com.google.inject.Inject
import com.typesafe.config.Config
import controllers.utils.{AccessLogging, Error}
import models.{Recommendation, RatingDAO, RecommendationEngine, RecommendationsDAO}
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.Json
import play.api.mvc._


class Recommendations @Inject() (
  config: Config,
  ratings: RatingDAO,
  recommendations: RecommendationsDAO,
  recommender: RecommendationEngine
) extends Controller with AccessLogging {

  def get(userId: Int, movieId: Int) = AccessLoggingAction.async {
    recommendations.lookup(userId, movieId).map { rec =>
      rec.map(r => Ok(Json.toJson(r)))
         .getOrElse(NotFound(Json.toJson(Error("Not found"))))
    }.recover {
      case e => InternalServerError(Json.toJson(Error(e.getMessage)))
    }
  }

  def clear() = Action.async {
    recommendations.deleteAll().map { _ =>
      Ok(Json.obj("message" -> "ok"))
    }.recover {
      case e => InternalServerError(Json.toJson(Error(e.getMessage)))
    }
  }

  def forUser(userId: Int) = Action.async {
    recommender.getUserRecommendations(userId).map { list =>
      Ok(Json.toJson(list))
    }.recover {
      case e: NoSuchElementException => Ok(Json.toJson(Seq.empty[Recommendation]))
    }
  }

  def topRatedMovies(count: Int, minRatings: Int) = Action {
    val movies = recommender.mostPopularMovies(count, minRatings)
    Ok(Json.toJson(movies))
  }

}
